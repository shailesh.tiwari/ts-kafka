/**
 * @desc [This provides a streaming connection to kafka server]
 */
import Debug from "debug";
import { Consumer, Kafka, Message, Producer } from "kafkajs";
import { ConnectionOptions } from ".";
import { Logger } from "@adhityan/gc-logger";

const debug = Debug("TSKafka:StreamingService");

export class StreamingService {
    private readonly kafka: Kafka;

    private readonly options: ConnectionOptions;

    private consumer: Consumer | undefined;

    private producer: Producer | undefined;

    private producerConnected: boolean;

    private consumerConnected: boolean;

    public constructor(options: ConnectionOptions) {
        debug("constructor", arguments);

        this.options = options;
        this.kafka = new Kafka(options.connection);
        this.producerConnected = this.consumerConnected = false;
    }

    public async openProducerChannel(): Promise<void> {
        if (this.producerConnected) return;
        this.producer = this.kafka.producer(this.options.producerOptions);
        await this.producer.connect();
        this.consumerConnected = true;
    }

    /**
     * publishes a message
     * @param subject
     * @param data
     */
    public async publish<T extends Message>(topic: string, data: T) {
        await this.openProducerChannel();
        await (<Producer>this.producer).send({ topic, messages: [data] });
    }

    public async openConsumerChannel(): Promise<void> {
        if (this.consumerConnected) return;
        this.consumer = this.kafka.consumer(this.options.consumerOptions);
        await this.consumer.connect();
        this.consumerConnected = true;
    }

    /**
     * subscribe, listens for messages on a channel
     * executes the "messageReceivedReference" method on receiving a message
     * @param channel : The channel/subject name
     * Expects json payload to be sent in message.data
     */
    public async subscribe(
        topic: string,
        messageReceived: (...args: any[]) => Promise<void>,
        messageValidation?: (...args: any[]) => Promise<boolean>,
        restartFromBeginning: boolean = false
    ) {
        await this.openConsumerChannel();

        await (<Consumer>this.consumer).subscribe({
            topic,
            fromBeginning: restartFromBeginning,
        });
        await (<Consumer>this.consumer).run({
            eachMessage: async ({ topic, partition, message: rawMessage }) => {
                try {
                    if (rawMessage.value == null) {
                        Logger.warn("Invalid null message ignored");
                        return;
                    }

                    debug("trying to parse message");
                    let message = JSON.parse(rawMessage.value.toString());

                    if (message.headers) {
                        rawMessage.headers = message.headers;
                        delete message.headers;
                    }

                    if (message.properties) {
                        rawMessage.attributes = message.properties;
                        delete message.properties;
                    }

                    if (message.body) {
                        debug(
                            "Setting final value as message.body. This is a side effect of how rd-kafka PHP sends messages"
                        );

                        if (
                            typeof message.body === "string" ||
                            message.body instanceof String
                        ) {
                            rawMessage.value = message.body;
                            message = JSON.parse(message.body);
                        } else {
                            message = message.body;
                        }
                    }

                    debug("parse message success", message);

                    try {
                        debug("call validation");
                        if (
                            !messageValidation ||
                            (messageValidation &&
                                (await messageValidation(message)))
                        ) {
                            debug("call validation success");

                            /* messageReceived gets the parsed message, passing subscription subscriptionOpts that it can be subscribed when required */
                            try {
                                debug("call message received processor");
                                await messageReceived(
                                    message,
                                    rawMessage.offset,
                                    partition,
                                    rawMessage.timestamp,
                                    topic
                                );
                                debug(
                                    "call message received processor success"
                                );
                            } catch (e) {
                                Logger.warn(
                                    "Unhandled exception in messageReceived",
                                    e
                                );
                            }
                        } else {
                            debug("call validation failed", message);
                        }
                    } catch (e) {
                        Logger.warn(
                            "Unhandled exception in messageValidation",
                            e
                        );
                    }
                } catch (e) {
                    Logger.warn(
                        "Invalid non-JSON message ignored",
                        rawMessage.value
                    );
                }
            },
        });
    }

    public async close() {
        await this.closeConsumer();
        await this.closeProducer();
    }

    public async closeConsumer() {
        await this.consumer?.disconnect();
    }

    public async closeProducer() {
        await this.producer?.disconnect();
    }
}
