import {
    ConsumerConfig,
    ConsumerSubscribeTopic,
    KafkaConfig,
    ProducerConfig,
    ProducerRecord,
} from "kafkajs";

export interface ConsumersConfig extends ConsumerConfig {
    restartFromBeginning?: boolean;
    channel?: string;
}

export interface ConnectionOptions {
    connection: KafkaConfig;
    consumerOptions?: ConsumersConfig;
    producerOptions?: ProducerConfig;
}

export interface PublishOptions extends ProducerRecord {}

export interface SubscriptionOptions extends ConsumerSubscribeTopic {}
