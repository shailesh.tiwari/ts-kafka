import Debug from "debug";
import { v4 as uuidv4 } from "uuid";
import { Tracer } from "@adhityan/gc-logger";

import { StreamingService } from "./kafka";
import { ConnectionOptions } from "./NSConnectionOptions.interface";

const debug = Debug("TSKafka:worker");

/**
 * StartWorker function, this creates a new connection and starts the subscription for a channel
 * @param connectionOptions : Kafka connection settings
 * @param channel : The channel/subject on which the worker will execute
 * @param messageReceived : This method needs to be defined in the Worker class.
 *   It will get executed whenever a message is received
 */
export async function startWorker<X extends { [key: string]: any }>(
    connectionOptions: ConnectionOptions,
    channel: string,
    messageReceived: (message: X) => Promise<void>,
    messageValidation?: (message: X) => Promise<boolean>
) {
    debug("startWorker called", channel, connectionOptions);
    let { connection, consumerOptions } = connectionOptions;

    if (!connection.clientId) {
        debug("No clientId provided; auto creating client ID");
        connection.clientId = uuidv4();
        debug(`auto set clientId to ${connection.clientId}`);
    }

    debug("client id being used", connection.clientId);
    const kafkaService = new StreamingService(connectionOptions);
    debug("kafkaService object created", kafkaService);

    debug("calling subscribe from startWorker actual", channel);
    return kafkaService.subscribe(
        channel,
        (message: X): Promise<void> => {
            return new Promise((resolve, reject) => {
                Tracer.setId(() => {
                    messageReceived(message).then(resolve).catch(reject);
                });
            });
        },
        messageValidation,
        consumerOptions?.restartFromBeginning
    );
}

/**
 * Worker factory
 * Executes the "messageReceived" method of the class whenever a message is received
 * @param connectionOptions : Kafka connection settings
 */
export function Worker<X extends { [key: string]: any }>(
    connectionOptions: ConnectionOptions
) {
    return function <
        T extends {
            new (...args: any[]): {
                messageReceived(message: X): Promise<void>;
                messageValidation?(message: X): Promise<boolean>;
            };
            channel?: string;
        }
    >(constructor: T) {
        debug("Worker outer wrap function being run");

        const channel =
            constructor.channel || connectionOptions.consumerOptions?.channel;
        if (!channel) {
            throw new Error("Invalid configuration: `Channel` not specified");
        }
        debug(`Using channel ${channel}`);

        const reference = class extends constructor {
            constructor(...args: any[]) {
                super(...args);
                debug("Worker wrapper constructor running");
                this.startWorker();
            }

            startWorker() {
                debug("Start worker called!");

                if (this.messageValidation) {
                    debug("Binding with message validation");
                    return startWorker(
                        connectionOptions,
                        channel,
                        this.messageReceived.bind(this),
                        this.messageValidation.bind(this)
                    );
                } else {
                    debug("Binding WITHOUT message validation");
                    return startWorker(
                        connectionOptions,
                        channel,
                        this.messageReceived.bind(this)
                    );
                }
            }
        };

        debug("Worker wrapper processing complete");
        return reference;
    };
}
