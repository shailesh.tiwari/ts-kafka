# ts-kafka

NOTE: This is a very opiniated library that uses a bunch of other internal libraries like gc-logger for Tracing and Logging support. We are planning to clean it up and open source it later.

A TypeScript wrapper service for the [Kafka](https://kafka.apache.org/) based on [kafkaJs](https://www.npmjs.com/package/kafkajs) library from Confluent.

## Publishing messages

```javascript
import { Logger } from "@adhityan/gc-logger";
import { StreamingService } from "./src";

Logger.init({
    fileLoggerEnabled: false,
    serviceName: "test",
});

const stream = new StreamingService({
    connection: {
        brokers: ["pkc-l7pr2.ap-south-1.aws.confluent.cloud:9092"],
        sasl: {
            mechanism: "plain",
        },
        ssl: true,
    },
});

stream.publish("test", { value: '{"test":"ok"}' });
```

## Subscribing to messages from a channel/subject

```javascript
import { Logger } from "@adhityan/gc-logger";
import { Worker } from "./src";

Logger.init({
    fileLoggerEnabled: false,
    serviceName: "test",
});

@Worker({
    connection: {
        brokers: ["pkc-l7pr2.ap-south-1.aws.confluent.cloud:9092"],
        sasl: {
            mechanism: "plain",
        },
        ssl: true,
    },
    consumerOptions: {
        channel: "test",
        groupId: "test-consumer",
    },
})
class Test {
    public async messageReceived(message: any) {
        Logger.debug(message);
    }
}

new Test();
```
