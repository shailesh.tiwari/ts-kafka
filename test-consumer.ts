import { Logger } from "@adhityan/gc-logger";
import { Worker } from "./src";

Logger.init({
    fileLoggerEnabled: false,
    serviceName: "test",
});

@Worker({
    connection: {
        brokers: ["pkc-l7pr2.ap-south-1.aws.confluent.cloud:9092"],
        sasl: {
            mechanism: "plain",
            username: "4RCTMKXSLHHWMUHR",
            password:
                "czcDfKQe2IOXJqC1AM+gtBdTMjkXxE0RPsSTboqppdxfHZysSPtssvytqIPIkVDX",
        },
        ssl: true,
    },
    consumerOptions: {
        channel: "test",
        groupId: "test-consumer",
    },
})
class Test {
    public async messageReceived(message: any) {
        Logger.debug(message);
    }
}

new Test();
