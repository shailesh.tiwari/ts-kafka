import { Logger } from "@adhityan/gc-logger";
import { StreamingService } from "./src";

Logger.init({
    fileLoggerEnabled: false,
    serviceName: "test",
});

const stream = new StreamingService({
    connection: {
        brokers: ["pkc-l7pr2.ap-south-1.aws.confluent.cloud:9092"],
        sasl: {
            mechanism: "plain",
            username: "4RCTMKXSLHHWMUHR",
            password:
                "czcDfKQe2IOXJqC1AM+gtBdTMjkXxE0RPsSTboqppdxfHZysSPtssvytqIPIkVDX",
        },
        ssl: true,
    },
});

stream.publish("test", {
    value: '{ "body": {"storeCode":"paramstest11","kycStatus":1,"ordersCount":"1","ordersSumAmount":"10.00000000"}, "properties": [], "headers": [] }',
});
